import 'package:dio/dio.dart';
import 'package:dio_demo_flutter/http_service.dart';
import 'package:dio_demo_flutter/models/single_user_model.dart';
import 'package:dio_demo_flutter/models/user_model.dart';
import 'package:flutter/material.dart';

class SingleUserPage extends StatefulWidget {
  const SingleUserPage({Key? key}) : super(key: key);

  @override
  State<SingleUserPage> createState() => _SingleUserPageState();
}

class _SingleUserPageState extends State<SingleUserPage> {
  late HttpService httpService;

  late SingleUserModel singleUserModel;
  late UserModel user;

  bool isLoading = false;

  Future getUser() async {
    Response response;
    try {
      isLoading = true;

      response = await httpService.getRequest('/api/users/2');

      isLoading = false;

      if (response.statusCode == 200) {
        setState(() {
          singleUserModel = SingleUserModel.fromJson(response.data);
          user = singleUserModel.userModel;
        });
      } else {
        print('There are some problems status code not 200');
      }
    } on Exception catch (e) {
      isLoading = false;
      print(e);
    }
  }

  @override
  void initState() {
    httpService = HttpService();
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Get Single User'),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : user != null
                ? Container(
                    width: double.maxFinite,
                    child: Column(
                      children: [
                        Image.network(user.avatar),
                        Container(
                          height: 16,
                        ),
                        Text("Hello, ${user.first_name} ${user.last_name}")
                      ],
                    ),
                  )
                : Center(
                    child: Text('No user founded!'),
                  ));
  }
}
