import 'package:dio_demo_flutter/models/user_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'single_user_model.g.dart';

@JsonSerializable()
class SingleUserModel {
  @JsonKey(name: "data")
  late UserModel userModel;

  SingleUserModel();

  factory SingleUserModel.fromJson(Map<String, dynamic> json) => _$SingleUserModelFromJson(json);
  Map<String, dynamic> toJson() => _$SingleUserModelToJson(this);
}
