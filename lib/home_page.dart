import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TextButton(
            onPressed: () {
              getHttps();
            },
            child: Text('Call')),
      ),
    );
  }

  Future<void> getHttps() async {
    Response response;
    var dio = Dio();
    response = await dio.get('/test?id=12&name=wendu');
    print(response.data.toString());
// Optionally the request above could also be done as
    response =
        await dio.get('/test', queryParameters: {'id': 12, 'name': 'wendu'});
    print(response.data.toString());
  }
}
